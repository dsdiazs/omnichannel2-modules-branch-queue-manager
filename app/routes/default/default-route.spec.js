const mockery = require('mockery');
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

describe('default-route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
  });
  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('default-route', () => {
    it('should the path be /', (done) => {
      const expressMock = {
        Router: () => {
          return {
            post: (path, callback) => {
              callback();
              expect(path).to.equal('/');
            }
          };
        }
      };
      mockery.registerMock('express', expressMock);

      const mockResponse = {
        util: {
          commonResponse: {
            sendResponseWithError: (res, code, message) =>  done() 
          }
        }
      };
      mockery.registerMock('omnichannel2-lib-common', mockResponse);
      //sinon.spy(mockResponse.util.commonResponse, 'sendResponseWithError');
      require('./default-route');
    });
  });
});
