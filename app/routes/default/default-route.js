const Router = require('express').Router;

const { sendResponseWithError } = require('omnichannel2-lib-common').util.commonResponse;

module.exports = Router().post('/', (req, res) => { return sendResponseWithError(res, 404, 'the version is not correct or was not defined'); });

