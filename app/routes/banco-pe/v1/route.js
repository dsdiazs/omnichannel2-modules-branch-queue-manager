const Router = require('express').Router;
const config = require('../../../config/index');

const { PassthroughMiddleware } = require('../../../middlewares/common/passthrough-middleware');
const { ResponseMiddleware } = require('../../../middlewares/common/response-middleware');

const router = Router();

const middlewares = [];
middlewares.push(PassthroughMiddleware);
middlewares.push(ResponseMiddleware);

router.post('/', middlewares);

module.exports = router;
