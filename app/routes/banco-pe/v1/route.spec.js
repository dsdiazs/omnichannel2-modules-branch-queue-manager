/* eslint-disable global-require */
const mockery = require('mockery');
const expect = require('chai').expect;

describe('route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
  });
  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('checking the called routes', () => {
    it('All the middlewares must be loaded when techlogTrace is true', () => {
      const routerMock = {
        Router: () => {
          return {
            post(path, middlewares) {
              expect(middlewares.length).to.be.equal(2);
            }
          }
        }
      }
      mockery.registerMock('../../../middlewares/common/passthrough-middleware', (req, res, next) => {});
      mockery.registerMock('../../../middlewares/common/response-middleware', (req, res, next) => {});
      
      mockery.registerMock('express', routerMock);
      const router = require('./route');
    });

  });

  describe('checking the called routes with middleware CallToProducer', () => {
    it('All the middlewares must be loaded when config.msConfig.callToProducer in true', () => {
      
      mockery.registerMock('../../../config/index', {
        msConfig: {
          name: 'cliente-interaccion-generar',
          port: 3120,
          country: 'PE',
          commerce: 'BANCO',
          timeOut: 40000,
          callToProducer: true
        }
      });

      const routerMock = {
        Router: () => {
          return {
            post(path, middlewares) {
              expect(middlewares.length).to.be.equal(2);
            }
          }
        }
      }
      
      mockery.registerMock('../../../middlewares/common/passthrough-middleware', (req, res, next) => {});
      mockery.registerMock('../../../middlewares/common/response-middleware', (req, res, next) => {});
      
      mockery.registerMock('express', routerMock);
      const router = require('./route');
    });

  });



});