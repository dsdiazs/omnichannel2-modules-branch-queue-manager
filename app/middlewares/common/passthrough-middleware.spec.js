const assert = require("assert");
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const mockery = require('mockery');

describe('PassthroughMiddleware', () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('fif-common-node4-flow-info', {
      logger: {
        info() { },
        error() { },
        debug() {},
        warning() {}
      }
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  it('Mock passthrough-controller return an Resolve in method executeService. Should to validate catch exceptoin, because res locals is been sending with null', async () => {
    //Create Mock
    const mockResp = {
      message: 'ok'
    };
    const mockPassthroughController = function () { this.callService = (req) => Promise.resolve(mockResp)};
    mockery.registerMock('../../controllers/banco-pe/passthrough-controller', mockPassthroughController);

    const middleware = require('./passthrough-middleware').PassthroughMiddleware; // eslint-disable-line
    try {
      await middleware( null, null, null );
    } catch (e) {
      assert.equal(e.message, 'Cannot read property \'locals\' of null');
    }
  });

  it.skip('Mock passthrough-controller return an Resolve in method executeService. Return 00 from Passthrough. Should to validate that execute nextSpy Middleware', async () => {
    //Create Mock:
    const mockResp = {
      body:  {
        status: {
          code: 200
        },
        estadoOperacion: {
          codigoOperacion: '00' // Case ok
        }
      }
    }
    const passthroughControllerMock = function () { 
      this.callService = (req) => Promise.resolve(mockResp)
    };
    mockery.registerMock('../../controllers/banco-pe/passthrough-controller', passthroughControllerMock);

    //Call middleware:
    const request = {};
    const res = {
      locals: {
        status: {
          code: 'ok'
        }
      }
    }
    const nextSpy = sinon.spy();
    const middleware = require('./passthrough-middleware').PassthroughMiddleware; // eslint-disable-line
    await middleware(request, res, nextSpy);
    expect(nextSpy.called).to.be.true;
    assert.equal(200, res.locals.status.message.status.code);
    assert.equal('00', res.locals.status.message.estadoOperacion.codigoOperacion);
  });

  it('Mock passthrough-controller return an Resolve in method executeService. Return 01 from Passthrough. Should to validate that execute nextSpy Middleware', async () => {
    //Create Mock:
    const responseMock = {
      body:  {
        status: {
          code: 200
        },
        estadoOperacion: {
          codigoOperacion: '01'
        }
      }
    }
    const passthroughControllerMock = function () { 
      this.callService = (req) => Promise.resolve(responseMock)
    };
    mockery.registerMock('../../controllers/banco-pe/passthrough-controller', passthroughControllerMock);

    //Call middleware:
    const request = {};
    const res = {
      locals: {
        status: {
          code: 'ok'
        },
        passthrough: {
          code: '',
          message: ''
        }
      }
    }
    const nextSpy = sinon.spy();
    const middleware = require('./passthrough-middleware').PassthroughMiddleware; // eslint-disable-line
    await middleware(request, res, nextSpy);

    assert.equal('fail', res.locals.status.code);
    assert.equal('200', res.locals.status.message.status.code);
    assert.equal('01', res.locals.status.message.estadoOperacion.codigoOperacion);
    expect(nextSpy.called).to.be.true;

  });


  it.skip('Mock passthrough-controller return an Reject in method callService. Should to validate passthrough.code and passthrough.message in passthrough-middleware', async () => {
    //Create Mock:
    const mockResp = {
      message: 'errorRejectFromPassthroughController'
    };
    const passthroughControllerMock = function () { this.callService = (req) => Promise.reject(mockResp)};
    mockery.registerMock('../../controllers/banco-pe/passthrough-controller', passthroughControllerMock);

    //Call middleware:
    const req = {};
    const res = {
      locals: {
        status: {
          code: 'ok'
        }
      }
    }
    const nextSpy = sinon.spy();
    
    const middleware = require('./passthrough-middleware').PassthroughMiddleware; // eslint-disable-line
    await middleware(req, res, nextSpy);
    assert.equal(res.locals.status.code, 'error');
    assert.equal(res.locals.status.message, mockResp.message);
  });

});
