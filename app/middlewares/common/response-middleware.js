
const ResponseController = require('../../controllers/banco-pe/response-controller');
const { sendResponseWithError } = require('omnichannel2-lib-common').util.commonResponse;

const { logger } = require('fif-common-node4-flow-info');

const moduleName = 'ResponseMiddleware';
const method = 'main';

async function ResponseMiddleware(req, res) {
  console.log('[response-middleware.js] Init in ResponseMiddleware');
  logger.info({ module: moduleName, method, description: 'init', detail: '' });
  //console.log(res);


  console.log('[response-middleware.js] paso 700');
  console.log('[response-middleware.js] res.locals.status:::');
  console.log(res.locals);
  let response = '';
  try {
    // const status = res.locals.status;
    const status = res.locals.passthrough;
    
    //const { reportCenter } = res.locals;
    
    console.log('[response-middleware.js] paso 701');
    if (status.code === 'ok') {
      console.log('[response-middleware.js] paso 702');
      const controller = new ResponseController();
      response = controller.parserResponse(status.message);
      logger.debug({ module: moduleName, method, description: 'sending response to ingress', detail: response });
      return res.status(200).send(response);
    }
    if (status.code === 'fail') {
      const controller = new ResponseController();
      response = controller.parserResponse(status.message);
      logger.debug({ module: moduleName, method, description: 'sending response to ingress', detail: response });
      return res.status(200).send(response);
    }
    if (status.code === 'error') {
      logger.error({ module: moduleName, method, description: 'error', detail: status.message });
      return sendResponseWithError(res, 500, status.message);
    }
    
  } catch (e) {
    logger.error({ module: moduleName, method, description: 'error', detail: e.message });
    return sendResponseWithError(res, 500, e);
  }
}

module.exports.ResponseMiddleware = ResponseMiddleware;
