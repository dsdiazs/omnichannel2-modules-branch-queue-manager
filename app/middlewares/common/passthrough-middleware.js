// Controller
const PassthroughController = require('../../controllers/banco-pe/passthrough-controller');
const { logger } = require('fif-common-node4-flow-info');

const moduleName = 'PassthroughMiddleware';
const method = 'main';

function initializePassthrough() {
  const jsonPasstrough = {
    code: null,
    message: null
  };

  return jsonPasstrough;
}

async function PassthroughMiddleware(req, res, next) {
  console.log('[passthrough-middleware] Init in PassthroughMiddleware!!!');

  console.log('[passthrough-middleware] paso 1!!!');
  logger.info({ module: moduleName, method, description: 'init', detail: '' });
  console.log('[passthrough-middleware] paso 2!!!');
  //if (res.locals.status && res.locals.status.code === 'ok') {
    console.log('[passthrough-middleware] paso 3!!!');
    try {
      /*res = {
        locals: {
          status: {
          }
        }
      };*/
      res.locals.passthrough = initializePassthrough();
      console.log('[passthrough-middleware] paso 4!!!');
      
      const controller = new PassthroughController();
      console.log('[passthrough-middleware] paso 5!!!');
      const response = await controller.callService(req);
      console.log('*********************************************************');
      console.log('*********************************************************');
      console.log(response.body);
      console.log('*********************************************************');
      console.log('*********************************************************');
    

      if (response.body.estadoOperacion.codigoOperacion === '00') {
        console.log("[passthrough-middleware] Response is OK");
        console.log("[passthrough-middleware] //////");
        console.log(res.locals);
        console.log("[passthrough-middleware] //////");
        
        res.locals.passthrough.code = 'ok';
        res.locals.passthrough.message = response.body;
        /*res.locals.status = {
          code: 'ok',
          message: response.body
        }*/

        console.log("[passthrough-middleware] --------");
        console.log(res.locals);
        console.log("[passthrough-middleware] --------");

      } else {
        // console.log("[passthrough-middleware] Response is FAIL");
        res.locals.status.code = 'fail';
        res.locals.status.message = response.body;
      }
      logger.debug({ module: moduleName, method, description: 'controller response', detail: response.body });
    } catch (e) {
      console.log(e);      
      res.locals.passthrough.code = 'error';
      res.locals.passthrough.message = e.message;
      logger.error({ module: moduleName, method, description: 'controller response with error', detail: e.message });
    }
    logger.info({ module: moduleName, method, description: 'calling to next middleware', detail: '' });
  //}
  return next();
}

module.exports.PassthroughMiddleware = PassthroughMiddleware;
