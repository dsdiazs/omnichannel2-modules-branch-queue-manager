const assert = require("assert");
const mockery = require('mockery');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
//var mocks = require('node-mocks-http');
//const getReq = mocks.createRequest();

describe('ResponseMiddleware', () => {

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('fif-common-node4-flow-info', {
      logger: {
        debug() { },
        info() { },
        error() { },
        warn() { }
      }
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  it.skip('Should expect an error', async () => {
    try {
      const middleware = require('./response-middleware').ResponseMiddleware; // eslint-disable-line
      await middleware( null, null );
    } catch (e) {
      assert.equal(e.message, 'Cannot read property \'status\' of null')
    }
  });


  it.skip('Should expect a succes case ok', async () => {

    // Section Mock
    const returnMock = {
    };
    const parserResponseControllerMock = class {
      parserResponse(data) {
          return returnMock;
      }
    };
    mockery.registerMock('../../controllers/banco-pe/response-controller', parserResponseControllerMock);

    // Section Call Middleware 
    const request = {};
    const res = {
      locals: {
        status: {
          code: 'ok'
        }/*,
        passthrough: {
          code: 'ok',
          message: {
            data: 'some-data'
          }
        }*/
      },
      status: (code) => {
        return {
          send: (response) => {
            expect(code).to.be.equals(200);
          }
        }
      }
    };

    const parserResponseSpy = sinon.spy(parserResponseControllerMock.prototype, 'parserResponse');
    const nextSpy = sinon.spy();
    const middleware = require('./response-middleware').ResponseMiddleware; // eslint-disable-line
    await middleware(request, res, nextSpy);
    //assert.equal(res.locals.passthrough.code, 'ok');
    assert.equal(res.locals.status.code, 'ok');
    expect(parserResponseSpy.called).to.be.true;
  });


  it.skip('Should expect a succes case fail', async () => {

    // Section Mock
    const mockLogger = {
      logger: {
            debug() { },
            info() { },
            error() { },
            warn() { }
      },
      loggerUtilTime: {
            time() {
              return {
                start: () => { },
                finish: () => {
                  return { time: 1 };
                },
              };
            }
      }
    };
    mockery.registerMock('fif-common-node4-flow-info', mockLogger);
    const spyLogger = sinon.spy(mockLogger.logger, 'debug');


    // Section Mock
    const returnMock = {
    };
    const parserResponseControllerMock = class {
      parserResponse(data) {
          return returnMock;
      }
    };
    mockery.registerMock('../../controllers/banco-pe/response-controller', parserResponseControllerMock);

    // Section Call Middleware 
    const request = {};
    const res = {
      locals: {
        status: {
          code: 'fail'
        }
      },
      status: (code) => {
        return {
          send: (response) => {
            expect(code).to.be.equals(200);
          }
        }
      }
    };

    const parserResponseSpy = sinon.spy(parserResponseControllerMock.prototype, 'parserResponse');
    const nextSpy = sinon.spy();
    const middleware = require('./response-middleware').ResponseMiddleware; // eslint-disable-line
    await middleware(request, res, nextSpy);
    // assert.equal(res.locals.passthrough.code, 'fail');
    assert.equal(res.locals.status.code, 'fail');
    expect(parserResponseSpy.called).to.be.true;

    expect(spyLogger.called).to.be.true;
    expect(spyLogger.callCount).to.be.equal(1);
  });


  it('Should expect a error code', async () => {


    // Section Mock
    const mockLogger = {
      logger: {
        debug() { },
        info() { },
        error() { },
        warn() { }
      },
      loggerUtilTime: {
        time() {
          return {
            start: () => { },
            finish: () => {
              return { time: 1 };
            },
          };
        }
      }
    };
    mockery.registerMock('fif-common-node4-flow-info', mockLogger);
    const spyLogger = sinon.spy(mockLogger.logger, 'error');


    // Section Mock
    const mockResp = {
      body:  {
        status: {
          code: 200
        }
      }
    };
    /*const mockCommonResp = {
      sendResponseWithError: (res, status, message) => {  }
    };*/

    // 
    const mockResponseWithError = {
      util: {
        commonResponse: {
          sendResponseWithError: (res, code, message) => {
            console.log('SEND RESPONSE');
            expect(code).to.be.equal(500);
            return { code : 500, message : 'Internal server error'};
          }
        }
      }
    };
    mockery.registerMock('omnichannel2-lib-common', mockResponseWithError);


    //const responseControllerMock = function () { this.parserResponse = (req) => Promise.resolve(mockResp)};
    //mockery.registerMock('../../controllers/banco-pe/response-controller', responseControllerMock);


    // Section Call Middleware 
    const request = {};
    const res = {
      locals: {
        status: {
          code: 'error',
          message: {
            data: 'some-data'
          }
        }
      },
      status: (code) => {
        return {
          send: (response) => {
            expect(code).to.be.equals(500);
          }
        }
      }
    };
    const nextSpy = sinon.spy();
    //const responseSpy = sinon.spy(mockResponseWithError, 'mockResponseWithError');
    const middleware = require('./response-middleware').ResponseMiddleware; // eslint-disable-line
    await middleware(request, res, nextSpy);
    assert.equal(res.locals.status.code, 'error');

    expect(spyLogger.called).to.be.true;
    expect(spyLogger.callCount).to.be.equal(1);

  });



  it('Should to be in catch', async () => {

    // Section Mock
    const mockLogger = {
      logger: {
        debug() { },
        info() { },
        error() { },
        warn() { }
      },
      loggerUtilTime: {
        time() {
          return {
            start: () => { },
            finish: () => {
              return { time: 1 };
            },
          };
        }
      }
    };
    mockery.registerMock('fif-common-node4-flow-info', mockLogger);
    const spyLogger = sinon.spy(mockLogger.logger, 'error');


    // Section Mock
    const mockResp = {
      body:  {
        status: {
          code: 200
        }
      }
    };

    // 
    const mockResponseWithError = {
      util: {
        commonResponse: {
          sendResponseWithError: (res, code, message) => {
            console.log('Send Response With Error');
            expect(code).to.be.equal(500);
            return { code : 500, message : 'Internal server error'};
          }
        }
      }
    };
    mockery.registerMock('omnichannel2-lib-common', mockResponseWithError);

    //const responseControllerMock = function () { this.parserResponse = (req) => Promise.resolve(mockResp)};
    //mockery.registerMock('../../controllers/banco-pe/response-controller', responseControllerMock);

    // Section Call Middleware 
    const request = {};
    const res = {
      locals: {
        /*status: {
          code: 'error',
          message: {
            data: 'some-data'
          }
        }*/
      },
      status: (code) => {
        return {
          send: (response) => {
            expect(code).to.be.equals(500);
          }
        }
      }
    };
    const nextSpy = sinon.spy();
    //const responseSpy = sinon.spy(mockResponseWithError, 'mockResponseWithError');
    const middleware = require('./response-middleware').ResponseMiddleware; // eslint-disable-line
    await middleware(request, res, nextSpy);
    //assert.equal(res.locals.status.code, 'error');

    expect(spyLogger.called).to.be.true;
    expect(spyLogger.callCount).to.be.equal(1);

  });

});