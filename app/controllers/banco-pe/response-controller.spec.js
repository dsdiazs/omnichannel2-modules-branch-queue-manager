//var ResponseController = require("./response-controller");

const assert = require("assert");
const sinon = require('sinon');
const mockery = require('mockery');
const expect = require('chai').expect;


beforeEach(() => {
  req = {
    headers: {
      country: 'PE',
      commerce: 'Banco',
      channel: 'Web'
    }
  };

  mockery.enable({
    warnOnReplace: false,
    warnOnUnregistered: false,
    useCleanCache: true
  });

  mockery.registerMock('fif-common-node4-flow-info', {
    logger: {
      info() { },
      error() { },
      debug() { }
    }
  });

});

afterEach(() => {
  mockery.disable();
  mockery.deregisterAll();
});

describe(" UNIT TEST: [app/controllers/banco-pe/response-controller.js] ", function () {
  it(" [parserResponse] Case: status code 200, without data.", function () {
    const Controller = require('./response-controller');
    const controller = new Controller();

    //Section Call Function
    let reportCenter = {
      code: 'ok',
      idTraceNumber: '123456789'
    };
    let responseInput = {
      estadoOperacion: {
        codigoOperacion: '00',
        glosaOperacion: 'OK data without products'
      }
    }
    
    let responseGenerated = controller.parserResponse(responseInput, reportCenter);
    console.log(responseGenerated);

    //Section Assert
    assert.equal(200, responseGenerated.status.code);
    assert.equal('ok', responseGenerated.status.message);
    assert.equal('error', responseGenerated.middleware.operationState.code);
    assert.equal('Error', responseGenerated.middleware.operationState.message);
    assert.equal(0, Object.keys(responseGenerated.middleware.data).length);
  });

  it.skip(" [parserResponse] Case: status code 200, with one product.", function () {
    //Section Expected Value:
    let expectedResponse = {
      status: {
        code: 200,
        message: 'ok'
      },
      middleware:
      {
        operationState: {
          code: '00',
          message: 'OK data with products'
        },
        data: {
          comprobante: {
            identificador: '1-13081785271',
            fechaHoraEmision: '2019-06-18T21:03:05.969Z'
          }
        }
      }
    };
    const Controller = require('./response-controller');
    const controller = new Controller();

    //Section Call Function
    let reportCenter = {
      code: 'ok',
      idTraceNumber: '123456789'
    };
    let responseInput = {
      estadoOperacion: {
        codigoOperacion: '00',
        glosaOperacion: 'OK data with products'
      },
      comprobante: {
        identificador: '1-13081785271',
        fechaHoraEmision: '2019-06-18T21:03:05.969Z'
      }
    };

    let responseGenerated = controller.parserResponse(responseInput, reportCenter);
    

    //Section Assert
    assert.equal(200 , responseGenerated.status.code);
    assert.equal('ok', responseGenerated.status.message);
    assert.equal('00', responseGenerated.middleware.operationState.code);
    assert.equal('OK data with products', responseGenerated.middleware.operationState.message);
    assert.equal('1-13081785271', responseGenerated.middleware.data.comprobante.identificador);
    assert.equal('123456789', responseGenerated.payload.reportCenterId);
  });

  it.skip(" [parserResponse] Case: status code 200, with one product, with reportCenter.code is not ok ", function () {
    //Section Expected Value:
    let expectedResponse = {
      status: {
        code: 200,
        message: 'ok'
      },
      middleware:
      {
        operationState: {
          code: '00',
          message: 'OK data with products'
        },
        data: {
          comprobante: {
            identificador: '1-13081785271',
            fechaHoraEmision: '2019-06-18T21:03:05.969Z'
          }
        }
      }
    };
    const Controller = require('./response-controller');
    const controller = new Controller();

    //Section Call Function
    let reportCenter = {
      code: 'error',
      idTraceNumber: '123456789'
    };
    let responseInput = {
      estadoOperacion: {
        codigoOperacion: '00',
        glosaOperacion: 'OK data with products'
      },
      comprobante: {
        identificador: '1-13081785271',
        fechaHoraEmision: '2019-06-18T21:03:05.969Z'
      }
    };

    let responseGenerated = controller.parserResponse(responseInput, reportCenter);
    

    //Section Assert
    assert.equal(200 , responseGenerated.status.code);
    assert.equal('ok', responseGenerated.status.message);
    assert.equal('00', responseGenerated.middleware.operationState.code);
    assert.equal('OK data with products', responseGenerated.middleware.operationState.message);
    assert.equal('1-13081785271', responseGenerated.middleware.data.comprobante.identificador);
    assert.equal('', responseGenerated.payload.reportCenterId);
  });
});
