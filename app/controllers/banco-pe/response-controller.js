const { _ } = require('lodash');
const { logger } = require('fif-common-node4-flow-info');

const moduleName = 'ResponseController';
const method = 'parserResponse';

class ResponseController {
  parserResponse(response) {
    console.log('[response-controller.js] Init in method parserResponse');
    logger.debug({ module: moduleName, method, description: 'Init in method parserResponse' });

    /*let reportCenterIdTraceNumber = 'default';
    if (reportCenter.code === 'ok') {
      reportCenterIdTraceNumber = reportCenter.idTraceNumber;
    } else {
      reportCenterIdTraceNumber = '';
    }*/

    const notMDWResponse = {
      codigoOperacion: '02',
      glosaOperacion: 'No hubo respuesta del middleware'
    };

    const statusOk = {
      code: 200,
      message: 'ok'
    };

    const passRes = _.get(response, 'estadoOperacion', notMDWResponse);
    const data = _.omit(response, 'estadoOperacion');

    const date = new Date();
    const timestamp = date.getTime();

    const commonResponse = {
      status: statusOk,
      middleware: {
        operationState: {
          code: passRes.codigoOperacion,
          message: passRes.glosaOperacion
        },
        data
      },
      payload: {
        dateTime: timestamp,
      }
    };

    if (_.isEmpty(data)) {
      commonResponse.middleware.operationState.code = 'error';
      commonResponse.middleware.operationState.message = 'Error';
    }

    logger.debug({ module: moduleName, method, description: 'End of method pasrserResponse - value commonResponse: ', detail: commonResponse });
    return commonResponse;
  }
}
module.exports = ResponseController;
