const config = require('../../config/index');
const { BaseController } = require('omnichannel2-lib-common').controller;
const { logger, loggerUtilTime } = require('fif-common-node4-flow-info');

const moduleName = 'PassthroughController';

class PassthroughController extends BaseController {

  async callService(req) {
    
    console.log('[passthrough-controller] paso 600!!!');

    const method = 'executeService';
    const utilTime = loggerUtilTime.time();
    utilTime.start();
    console.log('[passthrough-controller] paso 601!!!');
    try {
      console.log('[passthrough-controller] paso 602!!!');
      const uri = config.services.clienteModuloAtencionObtener;
      console.log(uri);
      console.log('[passthrough-controller] paso 603!!!');
      // const headers = this.createHeaders(req, true);
      const headers = {
        country: 'AR',
        commerce: 'CMR',
        channel: 'Web'
      };
      console.log( req.locals );
      console.log('[passthrough-controller] paso 604!!!');
      // const body = req.locals.payload;
      const identificador = req.body.sucursal.identificador;
      console.log(identificador);

      const body = {
        "sucursal": {
          "identificador": identificador
        }
      };

      console.log('[passthrough-controller] paso 605!!!');
      logger.debug({ module: moduleName, method, description: 'The controller uri and headers', detail: { uri, headers: req.headers } });
      const response = await this.createRequest(uri, headers, body, config.msConfig.timeOut);
      console.log('[passthrough-controller] paso 606!!!');
      console.log( response );

      logger.info({ module: moduleName, method, description: 'The controller responded in', detail: utilTime.finish().time });
      logger.info({ module: moduleName, method, description: 'The controller response: ', detail: response });
      return response;
    } catch (e) {
      logger.error({ module: moduleName, method, description: 'The controller responded with error in', detail: utilTime.finish().time });
      logger.error({ module: moduleName, method, description: 'The controller response:', detail: e.message });
      throw e;
    }
  }

}

module.exports = PassthroughController;
