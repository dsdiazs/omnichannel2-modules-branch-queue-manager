const assert = require("assert");
const sinon = require('sinon');
const mockery = require('mockery');
const expect = require('chai').expect;

let sandbox;

/////////////////////////////////////////////////////////////////////////////////////
describe(" UNIT TEST: [app/controllers/banco-pe/passthrough-controller.js] ", function(){
        
    describe(" Method executeService ", function(){

        beforeEach( function() {

            sandbox = sinon.createSandbox();
        
            mockery.enable({
                warnOnReplace: false,
                warnOnUnregistered: false,
                useCleanCache: true
            });
        
            mockery.registerMock('../../config/index', {
                msConfig: {
                    name: 'cliente-interaccion-generar',
                    port: 3120,
                    country: 'PE',
                    commerce: 'BANCO',
                    timeOut: 40000,
                    requireToken: true
                  },
                  services: {
                    api: 'http://localhost:3022/complaints/api/v1',
                    clienteInteraccionGenerar: 'http://localhost:3021/complaints/passthrough/cliente-interaccion-generar',
                    reportCenter: 'http://localhost:9003/api/reportCenter',
                    clienteNotificacionEnviar: 'http://localhost:3021/complaints/passthrough/cliente-notificacion-enviar'
                  },
                  fifLogConfig: {
                    fileName: 'fif-log-config-default',
                    levelDev: process.env.LOG_LEVEL_DEV || 'debug',
                    levelQa: process.env.LOG_LEVEL_QA || 'debug',
                    levelProd: process.env.LOG_LEVEL_PROD || 'info',
                    fileNameFormat: process.env.LOG_FILE_NAME || 'hostname-service-date-level',
                    outputFormat: process.env.LOG_OUTPUT_FORMAT || 'elk-message'
                  }
            });
        
            mockery.registerMock('fif-common-node4-flow-info', {
                logger: {
                    debug() { },
                    info() { },
                    error() { },
                    warn() { }
                },
                loggerUtilTime: {
                  time() { 
                      return {
                        start: () => {},
                        finish: () => {
                            return { time:1 };
                        }
                      };
                  }
                }
            });
          
        });
        
        afterEach( function() {
            sandbox.restore()
        
            mockery.disable();
            mockery.deregisterAll();
        });
        

        
        it.skip('Case: Promise Reject', async () => {

            // Section: Create Mock
            const mockResponseCreateRequest = { message: 'error' };
            const mockResponseCreateHeaders = {  };
            // Mockery, with extends BaseController
            const mockLibCommon = {
                controller: {
                    BaseController: function () { 
                        this.createRequest = (uri, headersMock, reqBody, timeOut) => Promise.reject(mockResponseCreateRequest),
                        //this.createHeaders = (headers, true) => return mockResponseCreateHeaders;
                        //this.createRequest = function(uri, headersMock, reqBody, timeOut) { return Promise.reject(mockResponseCreateRequest)} ,
                        //this.createHeaders = function(headers, booleano) { return mockResponseCreateHeaders }
                        //this.createHeaders = function(headers, booleano) { return mockResponseCreateHeaders }
                        this.createHeaders = (req, modifyHeaders) => {
                            assert.equal(true, modifyHeaders);
                            return Promise.resolve(mockResponseCreateHeaders);
                        }

                    }
                }
            }
            mockery.registerMock('omnichannel2-lib-common', mockLibCommon);

            // This section must be after mockery.registerMock
            const ControllerPassthrough = require('./passthrough-controller');
            const controllerPassthrough = new ControllerPassthrough();
            //sinon.stub(controllerPassthrough, 'createRequest').returns(Promise.resolve( mockResponseCreateRequest ));

            // Section: Execute function
            let req = {
                headers : {
                    country: "PE",
                    commerce: "Commerce",
                    channel: "WEB"
                },
                locals : {
                  payload: {

                  }
                }
            };
            let error = null;
            try {  
                response = await controllerPassthrough.callService( req );
            } catch (e) {
                error = e;
            }
            expect(error).to.be.not.null;
            expect(error.message).to.be.equals('error');

        });

        
        it.skip('Case: Promise Resolve', async () => {
/*
            const mockRequestPromise = (options) => { 
                assert.equal(true, options.json);
                assert.equal(true, options.resolveWithFullResponse);
                return Promise.resolve( mockResponseRP );
            };
            mockery.registerMock('request-promise', mockRequestPromise);
*/

            // Section: Create Mock
            const mockResponseCreateRequest = "OK";
            const mockResponseCreateHeaders = {
                headers : {
                    country: "PE",
                    commerce: "Commerce",
                    channel: "WEB"
                }
            };
            // Mockery, with extends BaseController
            const mockLibCommon = {
                controller: {
                    BaseController: function () { 
                        this.createRequest = (uri, headersMock, reqBody, timeOut) => Promise.resolve(mockResponseCreateRequest),
                        //this.createHeaders = (req, modifyHeaders) => Promise.resolve();
                        this.createHeaders = (req, modifyHeaders) => {
                            assert.equal(true, modifyHeaders);
                            return Promise.resolve(mockResponseCreateHeaders);
                        }
                        //this.createHeaders = function(headers, booleano) { return mockResponseCreateHeaders }
                    }
                }
            }
            mockery.registerMock('omnichannel2-lib-common', mockLibCommon);

            // This section must be after mockery.registerMock
            const ControllerPassthrough = require('./passthrough-controller');
            const controllerPassthrough = new ControllerPassthrough();
            //sinon.stub(controllerPassthrough, 'createRequest').returns(Promise.resolve( mockResponseCreateRequest ));

            // Section: Execute function
            let req = {
                headers : {
                    country: "PE",
                    commerce: "Commerce",
                    channel: "WEB"
                },
                locals : {
                  payload: {

                  }
                }
            };
            
            response = await controllerPassthrough.callService( req );
            assert.equal(mockResponseCreateRequest, "OK" );

            //assert.equal(mockResponseCreateRequest, "OK" );

        });

    });

});