const mockery = require('mockery');
const sinon = require('sinon');
const chai = require('chai');

const expect = chai.expect;

process.env.NODE_ENV_APP = 'banco-pe';
process.env.NODE_ENV = 'local';

describe('index config', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('index', () => {
    it('should throw an exception when then config folder does not exist', () => {
      const fsMock = {
        existsSync: () => {
          if (process.env.NODE_ENV_APP === 'no-folder') {
            return false;
          }
          return true;
        }
      };
      mockery.registerMock('fs', fsMock);      
      process.env.NODE_ENV_APP = 'no-folder';
      expect(() => { require('./index'); }).to.throw(Error);
      
      
    });

    it('should throw an exception when then config file does not exist', () => {
      const fsMock = {
        existsSync: (url) => {
          if (url.indexOf('other_config') >= 0) {
            return false;
          }
          return true;
        }
      };
      mockery.registerMock('fs', fsMock);
      process.env.NODE_ENV_APP = 'default';
      process.env.NODE_ENV = 'other_config';
      expect(() => { require('./index'); }).to.throw(Error);
    });

    it('should return the local configuration for banco-pe', () => {
      const fsMock = {
        existsSync: () => {
          return true;
        }
      };
      mockery.registerMock('fs', fsMock);
      const spy = sinon.spy(fsMock, 'existsSync');
      process.env.NODE_ENV_APP = 'banco-pe';
      process.env.NODE_ENV = 'local';
      const config = require('./index');
      expect(config).to.be.an('object');
      expect(config.env).to.be.equal('local');
      expect(config.envApp).to.be.equal('banco-pe');
      expect(spy.called).to.be.true
    });
  });
});