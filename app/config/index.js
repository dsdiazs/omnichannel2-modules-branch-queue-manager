const fs = require('fs');

const env = process.env.NODE_ENV || 'local'; // || 'local';
const envApp = process.env.NODE_ENV_APP || 'default'; // || 'default';

if (!fs.existsSync(`${__dirname}/${envApp}/`)) {
  throw new Error(`the folder ${__dirname}/${envApp}/ was not found, set correctly the env variable NODE_ENV_APP`);
}

const configPath = `${__dirname}/${envApp}/config.${env}`;
if (!fs.existsSync(configPath) && !fs.existsSync(`${configPath}.js`)) {
  throw new Error(`the config file ${__dirname}/${envApp}/config.${env} was not found, set correctly the env variable NODE_ENV`);
}

const cfg = require(`./${envApp}/config.${env}`); // eslint-disable-line
let commonConfig = null;
commonConfig = cfg;
commonConfig.env = env;
commonConfig.envApp = envApp;

process.env.RSA_2048_PRIVATE = './app/config/rsa_2048_priv_test.pem';

module.exports = Object.freeze(commonConfig);
