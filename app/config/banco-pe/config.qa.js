module.exports = {
  msConfig: {
    name: 'cliente-modulo-atencion-obtener',
    port: 9120,
    country: 'ARG',
    commerce: 'CMR',
    timeOut: 40000
  },
  services: {
    clienteModuloAtencionObtener: 'http://172.17.0.1:9021/api/branchqueue/passthrough/cliente-modulosatencion-obtener'

  },
  fifLogConfig: {
    fileName: 'fif-log-config-default',
    levelDev: process.env.LOG_LEVEL_DEV || 'debug',
    levelQa: process.env.LOG_LEVEL_QA || 'debug',
    levelProd: process.env.LOG_LEVEL_PROD || 'info',
    fileNameFormat: process.env.LOG_FILE_NAME || 'hostname-service-date-level',
    outputFormat: process.env.LOG_OUTPUT_FORMAT || 'elk-message'
  }
};
