
const setLoggerConf = require('fif-common-node4-flow-info').setLoggerConf;
const loggerInit = require('fif-common-node4-flow-info').loggerInit;
const config = require('../../config/index');

const loggerFIFConfig = {
  env: process.env.NODE_ENV,
  envName: 'NODE_ENV',
  envDefault: 'local',
  logLevelEnv: { local: 'debug', development: 'debug', qa: 'debug', production: 'info' },
  filePath: { local: '/tmp/', development: '/var/log/pm2/', qa: '/var/log/pm2/', production: '/var/log/pm2/' },
  fileName: config.fifLogConfig.fileNameFormat,
  envToFile: ['development', 'qa', 'production'],
  envToConsole: ['local'],
  disableLogger: ['iso'],
  serviceName: config.msConfig.name,
  useFlowInfoFromHeaders: false,
  localLog: { active: true, filePath: 'logs/' },
  outputFormat: config.fifLogConfig.outputFormat
};

setLoggerConf(loggerFIFConfig);
// to log initial information
loggerInit.initLog({ init: 'starting fif Logger', config: loggerFIFConfig });

module.exports = loggerFIFConfig;
