
const mockery = require('mockery');
const chai = require('chai');

const expect = chai.expect;

describe('fif-log-config-default', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('fif-log-config-default', () => {
    /*it('Should return the log-fif config', () => {
      mockery.registerMock('fif-common-node4-flow-info', {
        logger: {
          debug: () => { },
          info: () => { },
          error: () => { }
        },
        setLoggerConf: () => {
        },
        loggerInit: {
          initLog: () => {
          }
        }
      });

      mockery.registerMock('../../config/index', {
        msConfig: {
          name: 'api-passthrough',
        },
        fifLogConfig: {
          fileName: 'fif-log-config-default',
          fileNameFormat: process.env.LOG_FILE_NAME || 'hostname-service-date-level',
        }
      })
      const configLog = require('./fif-log-config-default');
      expect(configLog.envDefault).to.be.equal('local');
      expect(configLog.serviceName).to.be.equal('api-passthrough');
      expect(configLog.envToConsole[0]).to.be.equals('local');
      expect(configLog.disableLogger[0]).to.be.equals('iso');
      expect(configLog.useFlowInfoFromHeaders).to.be.equals(false);
      expect(configLog.envToFile[0]).to.be.equals('development');
      expect(configLog.envToFile[1]).to.be.equals('qa');
      expect(configLog.envToFile[2]).to.be.equals('qa');
      expect(configLog.localLog.active).to.be.equals(true);
      expect(configLog.localLog.filePath).to.be.equals('logs/');
    });*/

    it('Should return true if the validation function receives true parameter', () => {
      mockery.registerMock('fif-common-node4-flow-info', {
        logger: {
          debug: () => { },
          info: () => { },
          error: () => { }
        },
        setLoggerConf: () => {
        },
        loggerInit: {
          initLog: () => {
          }
        }
      });
      mockery.registerMock('../../config/index', {
        msConfig: {
          name: 'api-passthrough',
        },
        fifLogConfig: {
          fileName: 'fif-log-config-default',
          fileNameFormat: process.env.LOG_FILE_NAME || 'hostname-service-date-level',
        }
      })
      const configLog = require('./fif-log-config-default');
      expect(configLog.envDefault).to.be.equal('local');
      expect(configLog.serviceName).to.be.equal('api-passthrough');
    });

    it('Should return false if the validation function receives false parameter', () => {
      mockery.registerMock('fif-common-node4-flow-info', {
        logger: {
          debug: () => { },
          info: () => { },
          error: () => { }
        },
        setLoggerConf: () => {
        },
        loggerInit: {
          initLog: () => {
          }
        }
      });
      mockery.registerMock('../../config/index', {
        msConfig: {
          name: 'api-passthrough',
        },
        fifLogConfig: {
          fileName: 'fif-log-config-default',
          fileNameFormat: process.env.LOG_FILE_NAME || 'hostname-service-date-level',
        }
      })
      const configLog = require('./fif-log-config-default');
      expect(configLog.envDefault).to.be.equal('local');
      expect(configLog.serviceName).to.be.equal('api-passthrough');
    });
  });
});
