module.exports = function (config) {
  config.set({
    mutate: [
      './app/**/!(config.*|*.spec).js',
    ],
    mutator: {
      name: 'javascript',
      excludedMutations: [
        'StringLiteral',
        'ObjectLiteral'
      ]
    },
    packageManager: "npm",
    //reporters: ["clear-text", "progress"],
    reporters: ["html", "progress"],
    testRunner: "mocha",
    transpilers: [],
    testFramework: "mocha",
    coverageAnalysis: "all",
    mochaOptions: {
      spec: ['./app/**/*.spec.js'],
      timeOut: 5000,
    }
  });
};
