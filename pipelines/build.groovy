#!groovy
env.IMAGE_BASE='registry.fif.tech/fif-common-pm2:11.14.0-3.5.0-1'
env.PROJECT_NAME='omnichannel2-modules-branch-queue-manager'
try {
  node('azure-slave') {
    notifyBuild('STARTED')

  	stage('Docker pull') {
  		docker.image('${IMAGE_BASE}').pull()
  	}

    stage('Clean Workspace') {
      docker.image('${IMAGE_BASE}').inside('-u root') {
        sh('find ${WORKSPACE} -mindepth 1 -delete')
      }
    }

    stage('Checkout') {
      checkout scm
    }
    
    stage('Detect Secrets') {       
      docker.image('registry.fif.tech/detect-secrets').inside('--entrypoint=""') {
        sh '''
            detect-secrets scan --exclude-files '(.git|venv|spec|lock.json)'
        '''
      }       
    }

    stage('Download dependencies') {
      docker.image('${IMAGE_BASE}').inside('-u root') {
        withCredentials([[$class: 'StringBinding', credentialsId: 'jenkins-npm', variable: 'PASSWORD']]) {
          sh '''
            echo -e 'registry=https://npm-registry.fif.tech/\n//npm-registry.fif.tech/:_authToken="'$PASSWORD'"\n@types:registry=https://registry.npmjs.org' > .cnpmrc
            npm install --userconfig=.cnpmrc --no-optional
            rm .cnpmrc
          '''
        }
      }
    }

    stage('Check dependency vulnerabilities') {
      dependencyCheckAnalyzer hintsFile: '', includeCsvReports: false, includeHtmlReports: true, includeJsonReports: false, isAutoupdateDisabled: true, outdir: '', scanpath: 'package*.json', skipOnScmChange: false, skipOnUpstreamChange: false, suppressionFile: '', zipExtensions: ''
      dependencyCheckPublisher canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', thresholdLimit: 'high', unHealthy: '', unstableTotalAll: '10', unstableTotalHigh: '1', unstableTotalNormal: '4'
    }

    stage('Run eslint'){
      docker.image('${IMAGE_BASE}').inside('-u root'){
        sh '''
          npm run eslint
        '''
      }
    }

    stage('Generate lcov'){
      docker.image('${IMAGE_BASE}').inside('-u root'){
        sh '''
          npm run lcov
        '''
      }
    }  

    stage('Unit-Test/Coverage over 80'){
      docker.image('${IMAGE_BASE}').inside('-u root'){
        sh '''
          npm run coverage-check
        '''
      }
    }

      

    stage('SonarQube analysis') {
      withSonarQubeEnv('sonar') {
        sh "/opt/sonar/bin/sonar-runner"
      }
    }   

    stage('Prune node_modules') {
      docker.image('${IMAGE_BASE}').inside('-u root') {
        sh '''
          npm prune --production
        '''
      }
    }

    stage('Calculate TAG') {
      try {
        env.GIT_TAG = sh(
          returnStdout: true,
          script: 'echo $(date +%Y%m%d)-$(date +%H%M%S)-BUILD'
        ).trim()
        echo "$GIT_TAG"
      } catch (error) {
        env.GIT_TAG = '1'
      }
    }

    stage('Building Docker Image') {
      sh '''
        docker build --rm=true -t "$PROJECT_NAME:$GIT_TAG" -f ${WORKSPACE}/dockerfiles/Dockerfile . --build-arg BUILDNUMBER=$GIT_TAG --label version=$GIT_TAG
      '''
    }

    stage('Anchore Analysis') {
      sh '''
        docker pull anchore/jenkins
        echo "$PROJECT_NAME:$GIT_TAG" > image.txt
      '''
      anchore bailOnWarn: false, inputQueries: [
        [query: 'cve-scan all'], [query: 'list-packages all'],
        [query: 'list-files all'], [query: 'show-pkg-diffs base']
      ], name: 'image.txt'
    }

    stage('Pushing Docker Image') {
      sh '''
        docker tag $PROJECT_NAME:$GIT_TAG registry.fif.tech/$PROJECT_NAME:$GIT_TAG
        docker tag $PROJECT_NAME:$GIT_TAG registry.fif.tech/$PROJECT_NAME:latest
        docker push registry.fif.tech/$PROJECT_NAME:$GIT_TAG
        docker push registry.fif.tech/$PROJECT_NAME:latest
      '''
    }

    stage('Cleaning Docker Images') {
      try {
        sh('docker rmi $(docker images -f "dangling=true" -q) --force')
      }catch(error){
        sh('echo "No hay Imagenes que borrar"')
      }
    }

    sh("git tag -f -a $GIT_TAG -m 'version $GIT_TAG'")
    sshagent(['11f132ab-f4f6-4c0e-9c87-8723dc0e47e7']) {
      sh("git push -f --no-verify --tags ")
    }
  }
} catch(e) {
   currentBuild.result = 'FAILURE'
   throw e
} finally {
  node('azure-slave') {
    stage('Clean Workspace') {
      docker.image('${IMAGE_BASE}').inside('-u root') {
        sh('find ${WORKSPACE} -mindepth 1 -delete')
      }
    }
    if (!currentBuild.result.equals('FAILURE')) {
      notifyBuild('SUCCESSFUL')
    } else {
      notifyBuild('FAILURE')
    }
  }
}

def notifyBuild(String buildStatus) {

  // Default values
  def colorCode = 'danger'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    colorCode = 'warning'
  } else if (buildStatus == 'SUCCESSFUL') {
    colorCode = 'good'
  } else {
    colorCode = 'danger'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary, channel: "#omnichannel2_notif");
}
