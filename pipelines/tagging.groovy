#!groovy
env.PROJECT_NAME='omnichannel2-modules-branch-queue-manager'
pipeline {
  agent {
    label 'azure-slave'
  }
  stages {
    stage ('Setting  solution') {
      steps {
        script {
          switch ("${SOLUTION}") {
            case "BFPE":
                env.ISOLUTION = 'bf-pe'
                break
            case "BFCL":
                env.ISOLUTION = 'bf-cl'
                break
            case "BFCO":
                env.ISOLUTION = 'bf-co'
                break
            case "CMRAR":
                env.ISOLUTION = 'cmr-ar'
                break
            case "CMRMX":
                env.ISOLUTION = 'cmr-mx'
                break
          }
        }
        sh ''' echo  SOLUTION $SOLUTION '''
      }
    }
    stage ('Setting environment') {
      steps {
        script {
          switch ("${ENVIRONMENT}") {
            case "TEST":
                env.IENVIRONMENT = 'dev'
                break
            case "QA":
                env.IENVIRONMENT = 'qa'
                break
            case "PROD":
                env.IENVIRONMENT = 'prod'
                break
          }
        }
        sh ''' echo  SOLUTION $SOLUTION '''
      }
    }
    stage('Promote image') {
      steps {
        sh '''
          if [ $IENVIRONMENT = "dev" ]; then
            docker pull registry.fif.tech/$PROJECT_NAME:$GIT_TAG
            docker tag registry.fif.tech/$PROJECT_NAME:$GIT_TAG registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG
            docker tag registry.fif.tech/$PROJECT_NAME:$GIT_TAG registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-latest
            docker push registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG
            docker push registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-latest
            docker rmi registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG
            docker rmi registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-latest
          elif [ $IENVIRONMENT = "qa" ]; then
            docker pull registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG
            docker tag registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-$GIT_TAG
            docker tag registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-latest
            docker push registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-$GIT_TAG
            docker push registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-latest
            docker rmi registry.fif.tech/$PROJECT_NAME:$ISOLUTION-dev-$GIT_TAG
          else
            docker pull registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-$GIT_TAG
            docker tag registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-$GIT_TAG registry.fif.tech/$PROJECT_NAME:$ISOLUTION-prod-$GIT_TAG
            docker tag registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-$GIT_TAG registry.fif.tech/$PROJECT_NAME:$ISOLUTION-prod-latest
            docker push registry.fif.tech/$PROJECT_NAME:$ISOLUTION-prod-$GIT_TAG
            docker push registry.fif.tech/$PROJECT_NAME:$ISOLUTION-prod-latest
            docker rmi registry.fif.tech/$PROJECT_NAME:$ISOLUTION-qa-$GIT_TAG
          fi
        '''
        sh ''' echo  Tag $GIT_TAG '''
      }
    }
    stage ('Triggering stack') {
      steps {
        script {
          echo ("DISABLE_DEPLOY: ${DISABLE_DEPLOY}")
          if ("YES" != "${DISABLE_DEPLOY}") {
            def jobName = "${ISOLUTION}".replaceAll('-', '/')
            build job: "omnichannel2-turnero/stacks/services/${jobName}/pipelines/${IENVIRONMENT}", propagate: true, wait: true
          }
        }        
      }      
    }
    stage('Summary') {  
      steps {
        script {
          echo("solution: ${SOLUTION}, environment: ${ENVIRONMENT}, tag: ${GIT_TAG}")
        }
      }      
    }
  }
}
