const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
// const Ddos = require('fif-common-node-dos');
const config = require('./app/config/index');
const RouteDefault = require('./app/routes/default/default-route');

const RouteV1 = require(`./app/routes/${config.envApp}/v1/route`); // eslint-disable-line

require(`./app/loggers/fif-log-config/${config.fifLogConfig.fileName}`); // eslint-disable-line

const app = express();

app.set('trust proxy', true);

// const ddos = new Ddos({ burst: 10, limit: 15 }, true);
// app.use(ddos.express);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());

// health check MS
app.get('/turnero/api/clienteModuloAtencionObtener/health', (req, res) => {
  res.send(`${config.msConfig.name} up and running`);
});

app.use('/turnero/api/clienteModuloAtencionObtener',
  RouteV1,
  RouteDefault);

app.use((err, req, res, next) => { // eslint-disable-line
  console.log(err);
  res.status(500).send({ code: 'error', message: 'internal error not handled' });
});

module.exports = app;
